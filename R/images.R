# use roxygen2:
# https://cran.r-project.org/web/packages/roxygen2/vignettes/rd.html
# run roxygen2::roxygenise() to generate documentation

#' Get the name of the main image of an hydrological station
#'
#' @export
#' @param stationCode String - The code of the station
#' @param lang String - Code of the language ("en", "fr", "es") for internationalized fields. Optional, default to "en"
#' @return The list of the images attached to the station
#' @examples
#' getImages("STATION_CODE")
#' getImages("STATION_CODE", "fr")
getImages <- function(stationCode, lang = "en") {
  checkLibraries()
  checkGlobalVariables()

  request <- paste0(getWimesHost(),
                    "/hydrometry/api/v1/",
                    get("WIMES_KEY", envir = .GlobalEnv),
                    "/hydrologicalStations/",
                    stationCode,
                    "/images",
                    "?lang=",
                    lang)

  return(requestAndParseJson(request))
}

#' Download an image identified by its name
#'
#' @export
#' @param name String - The code of the station
#' @param destfile String - Path where the method downloads the image (optional, default value temp folder)
#' @return The path of the downloaded image
#' @examples
#' downloadImage("STATION_CODE", "imageName")
#' downloadImage("STATION_CODE", "imageName", "monImage.jpeg")
downloadImage <- function(stationCode, imageName, destfile = tempfile()) {
  checkLibraries()
  checkGlobalVariables()

  request <- paste0(getWimesHost(),
                    "/hydrometry/api/v1/",
                    get("WIMES_KEY", envir = .GlobalEnv),
                    "/hydrologicalStations/",
                    stationCode,
                    "/images/",
                    imageName)

  download.file(request, destfile, "auto", quiet = TRUE, mode = "wb");

  return(destfile)
}