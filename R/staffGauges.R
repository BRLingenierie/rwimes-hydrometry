# use roxygen2:
# https://cran.r-project.org/web/packages/roxygen2/vignettes/rd.html
# run roxygen2::roxygenise() to generate documentation

#' Get all the staff gauges related to an hydrometric or hydrometeorological station
#'
#' @export
#' @param stationCode String - The code of the station
#' @param lang String - Code of the language ("en", "fr", "es") for internationalized fields. Optional, default to "en"
#' @return A data frame describing the staff gauges
#' @examples
#' getStaffGauges("STATION_CODE")
#' getStaffGauges("STATION_CODE", "fr")
getStaffGauges <- function(stationCode, lang = "en") {
  checkLibraries()
  checkGlobalVariables()

  request <- paste0(getWimesHost(),
                    "/hydrometry/api/v1/",
                    get("WIMES_KEY", envir = .GlobalEnv),
                    "/hydrometricStations/",
                    stationCode,
                    "/staffGauges",
                    "?lang=",
                    lang)

  return(requestAndParseJson(request))
}